<?php

/**
 * The function accepts a callback, it will only run in development
 * thus helping us debug.
 */
if(! function_exists('playground')) {
	function playground(Callable $callback) {
		if(config('app.env') === 'production') {
			return;
		}

		$callback();
	}
}
