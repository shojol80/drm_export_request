<?php

namespace FreightManager\Http\Controllers;

use Illuminate\Routing\Controller;
use FreightManager\Data\Export as ExportData;
use FreightManager\Services\FreightManagerService;
use Illuminate\Http\Request;

class Delete extends Controller
{
    protected $service;

    public function __construct(FreightManagerService $service)
    {
        $this->service = $service;
    }

    public function __invoke(Request $request)
    {
        $request->validate($this->rules());

        $data = $request->only(array_keys($this->rules()));

        $res = $this->service->handle(ExportData::map([
            'product_ids'  => $data['product_ids'],
            'merchant_id'  => (int) $data['merchant_id'],
            'credentials'  => $data['credentials'],
            'shop_url'     => $data['shop_url'],
            'channel'      => (int) $data['channel_id']
        ]),true);
        return $res;
        // return res($res->data ?? ['data' => null])->message($res->message)->code($res->success() ? 200 : 422)->send();
    }

    private function rules()
    {
        return [
            'product_ids'  => 'required',
            'merchant_id'  => 'required|numeric',
            'credentials'  => 'required',
            'shop_url'     => 'required|url',
            'channel_id'   => 'required|numeric'
        ];
    }
}
