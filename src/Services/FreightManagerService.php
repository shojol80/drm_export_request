<?php


namespace FreightManager\Services;

use Apiz\AbstractApi;
use FreightManager\Data\Export;
use GuzzleHttp\Exception\ClientException;
use FreightManager\Data\Response as ExportResponse;

class FreightManagerService extends AbstractApi
{
    private const CHANNEL_WOOCOMMERCE = 'woo_commerce';

    protected $prefix = 'api/v1';

    private $url = '/products';

    private $delete_url = '/delete-products';

	protected function baseUrl()
 	{
	 	return config('export.base_url');
 	}

	/**
	 * @param Export $export
	 * @param bool $shouldDelete
	 * @return ExportResponse
	 */
 	public function handle(Export $export,bool $shouldDelete)
    {
        $this->setupAuthenticationHeaders();

        $this->formParams($export->toArray());

        // try {

        	if($shouldDelete) {
		        $response = $this->delete($this->delete_url);
	        } else {
		        $response = $this->post($this->url);
	        }
          return $response->getContents();
            return ExportResponse::map(json_decode($response->getContents(),true));

        // } catch (ClientException $e) {
        //
        //     return ExportResponse::map(
        //         json_decode((string) $e->getResponse()->getBody(),true)
        //     );
        //
        // } catch (\Exception $e) {
        //     return ExportResponse::map(
        //         $this->mapException($e)
        //     );
        // }
    }

    private function mapException(\Exception $e)
    {
        return [
            'code' => 422,
            'status' => 'ERROR',
            'message' => $e->getMessage(),
        ];
    }

    private function setupAuthenticationHeaders()
    {
        $this->headers([
            'x-jl-authorization' => config('export.authorization_key')
        ]);
    }

}
