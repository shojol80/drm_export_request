<?php


namespace FreightManager\Data;

use FreightManager\Enums\Channels;
use Spatie\DataTransferObject\DataTransferObject;

class Response extends DataTransferObject
{
    /**
     * @var int
     */
    public $code;
    /**
     * @var string
     */
    public $status;
    /**
     * @var string
     */
    public $message;
    /**
     * @var array
     */
    public $data;

    public static function map(array $data)
    {
        return new self([ 
            'code'    => $data['code'],
            'status'  => $data['status'],
            'message' => $data['message'],
            'data'    => $data['data'] ?? [],
        ]);
    }

    public function success()
    {
        return $this->status === 'SUCCESS';
    }

    public function pipelineItemID()
    {
        if(! $this->success()) {
            return null;
        }

        return $this->data['pipeline_item_id'] ?? null;
    }
}
