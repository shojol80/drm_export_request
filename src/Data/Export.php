<?php


namespace FreightManager\Data;

use FreightManager\Enums\Channels;
use Spatie\DataTransferObject\DataTransferObject;

class Export extends DataTransferObject
{
    /**
     * @var array
     */
    public $product_ids;
    /**
     * @var string
     */
    public $url;
    /**
     * @var string
     */
    public $channel;
    /**
     * @var int
     */
    public $merchant_id;
    /**
     * @var array
     */
    public $credentials;


    public static function map(array $data)
    {
        return new self([
            'product_ids' => $data['product_ids'],
            'url'         => $data['shop_url'],
            'channel'     => self::mapChannel($data['channel']),
            'credentials' => $data['credentials'],
            'merchant_id' => $data['merchant_id']
        ]);
    }

    private static function mapChannel(int $channelID)
    {
        return Channels::MAP[$channelID];
    }
}
