<?php


namespace FreightManager\Enums;

interface Channels {
    const GAMBIO  = 1;
    const _GAMBIO = 'gambio';

    const EBAY  = 4;
    const _EBAY = 'e_bay';

    const AMAZON  = 5;
    const _AMAZON = 'amazon';

    const SHOPIFY  = 6;
    const _SHOPIFY = 'shopify';
    
    const WOO_COMMERCE = 7;
    const _WOO_COMMERCE = 'woo_commerce';

    const ETSY  = 11;
    const _ETSY= 'etsy';

    const OTTO  = 12;
    const _OTTO = 'otto';

    const KAUFLAND = 13;
    const _KAUFLAND = 'kaufland';

    const MAP = [
        self::WOO_COMMERCE => self::_WOO_COMMERCE,
        self::EBAY => self::_EBAY,
        self::AMAZON => self::_AMAZON,
        self::GAMBIO => self::_GAMBIO,
        self::SHOPIFY => self::_SHOPIFY,
        self::ETSY => self::_ETSY,
        self::OTTO => self::_OTTO,
        self::KAUFLAND => self::_KAUFLAND
    ];
}
